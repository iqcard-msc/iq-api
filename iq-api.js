define('iq-api.service',[], function () {
    'use strict';

    function $iqApiProvider() {
        var provider = this;

        provider.$get = $iqApi;

        provider.urlPrefix = '';

        $iqApi.$inject = ['$http', '$httpParamSerializerJQLike'];
        /* Service class */
        function $iqApi($http, $httpParamSerializerJQLike) {

            function iqApi(method, url, params) {
                params = params || {};
                params.method = method;
                params.url = provider.urlPrefix + url;

                return $http(params);
            }

            iqApi.getUrlPrefix = getUrlPrefix;
            iqApi.wrap = wrap;
            iqApi.postForm = postForm;

            return iqApi;

            ////////////////////

            function getUrlPrefix() {
                return provider.urlPrefix;
            }

            function wrap(method, url) {
                return function () {
                    return method.bind($http, provider.urlPrefix + url).apply(null, arguments);
                };
            }

            function postForm(url, data) {
                return iqApi('post', url, {
                    data: $httpParamSerializerJQLike(data),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                });
            }

            function putForm(url, data) {
                return iqApi('put', url, {
                    data: $httpParamSerializerJQLike(data),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                });
            }

        }
    }

    return $iqApiProvider;
});

define('iq-api.auth.service',[], function () {
    'use strict';

    $iqApiAuth.$inject = ['$q', '$rootScope', '$http', '$iq.api'];
    function $iqApiAuth($q, $rootScope, $http, $iqApi) {
        var service = {};

        service.login = login;
        service.logout = logout;
        service.getUserInfo = getUserInfo;
        service.hasPermissions = hasPermissions;

        return service;

        ////////////////////

        function login(data) {
            return $iqApi.postForm('/login', data);
        }

        function logout() {
            $rootScope.user = undefined;
            return $iqApi('post', '/logout');
        }

        function getUserInfo() {
            if ($rootScope.user) {
                return $q.when($rootScope.user);
            }
            return $iqApi('get', '/user/current')
                .then(function (response) {
                    $rootScope.user = response.data;
                    return response.data;
                });
        }

        function hasPermissions(arrayOfPermissions) {
            return getUserInfo().then(function (user) {
                if (!user.roles || !user.roles.length) {
                    return false;
                }
                var i;
                var result = false;
                for (i = 0; i < arrayOfPermissions.length; ++i) {
                    if (user.roles.indexOf(arrayOfPermissions[i]) !== -1) {
                        result = true;
                        break;
                    }
                }
                return result;
            });
        }
    }

    return $iqApiAuth;
});

define('iq-api.dict.service',[], function () {
    'use strict';

    $iqApiDict.$inject = ['$http', '$iq.api'];
    function $iqApiDict($http, $iqApi) {
        var service = {};

        service.getByName = getByName;
        service.getByEnumName = getByEnumName;

        return service;

        ////////////////////

        function getByName(name) {
            return $iqApi('get', '/dictionaries/' + name)
                .then(function (response) {
                    return response.data;
                });
        }

        function getByEnumName(enumName) {
            return $iqApi('get', '/dictionaries/enum/' + enumName)
                .then(function (response) {
                    return response.data;
                });
        }
    }

    return $iqApiDict;
});

define('iq-api.module',[
    'angular',
    './iq-api.service',
    './iq-api.auth.service',
    './iq-api.dict.service'
], function (
    angular,
    $iqApi,
    $iqApiAuth,
    $iqApiDict
) {
    'use strict';

    var app = angular.module('iq.api', []);

    app.provider('$iq.api', $iqApi);

    app.factory('$iq.api.auth', $iqApiAuth);
    app.factory('$iq.api.dict', $iqApiDict);

    return app;
});

define('iq-api',['./iq-api.module'], function () {
    'use strict';
});

