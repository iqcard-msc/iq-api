module.exports = function(grunt) {
    'use strict';

    grunt.initConfig({
        bowerConfig: grunt.file.readJSON('./bower.json'),

        requirejs: {
            compile: {
                options: {
                    baseUrl: './src/',
                    name: 'iq-api',
                    out: 'iq-api.js',
                    optimize: 'none',
                    paths: {
                        'angular': 'empty:',
                    }
                }
            }
        },
        watch: {
            default: {
                files: ['src/**/*.js'],
                tasks: ['requirejs']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('start', function () {
        require('./server.js');
        grunt.task.run('watch');
    });

    grunt.registerTask('default', ['requirejs']);
};
