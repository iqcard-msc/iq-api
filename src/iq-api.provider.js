define([], function () {
    'use strict';

    function IqApiProvider() {
        var provider = this;

        provider.$get = IqApiService;

        provider.urlPrefix = '';

        IqApiService.$inject = ['$http', '$httpParamSerializerJQLike'];
        /* Service class */
        function IqApiService($http, $httpParamSerializerJQLike) {

            function iqApi(method, url, params) {
                params = params || {};
                params.method = method;
                params.url = provider.urlPrefix + url;

                return $http(params);
            }

            iqApi.getUrlPrefix = getUrlPrefix;
            iqApi.wrap = wrap;
            iqApi.postForm = postForm;

            return iqApi;

            ////////////////////

            function getUrlPrefix() {
                return provider.urlPrefix;
            }

            function wrap(method, url) {
                return function () {
                    return method.bind($http, provider.urlPrefix + url).apply(null, arguments);
                };
            }

            function postForm(url, data) {
                return iqApi('post', url, {
                    data: $httpParamSerializerJQLike(data),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                });
            }

            function putForm(url, data) {
                return iqApi('put', url, {
                    data: $httpParamSerializerJQLike(data),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                });
            }

        }
    }

    return IqApiProvider;
});
