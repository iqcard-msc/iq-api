define([], function () {
    'use strict';

    $iqApiAuth.$inject = ['$q', '$rootScope', '$http', '$iq.api'];
    function $iqApiAuth($q, $rootScope, $http, $iqApi) {
        var service = {};

        service.login = login;
        service.logout = logout;
        service.getUserInfo = getUserInfo;
        service.hasPermissions = hasPermissions;

        return service;

        ////////////////////

        function login(data) {
            return $iqApi.postForm('/login', data);
        }

        function logout() {
            $rootScope.user = undefined;
            return $iqApi('post', '/logout');
        }

        function getUserInfo() {
            if ($rootScope.user) {
                return $q.when($rootScope.user);
            }
            return $iqApi('get', '/user/current')
                .then(function (response) {
                    $rootScope.user = response.data;
                    return response.data;
                });
        }

        function hasPermissions(arrayOfPermissions) {
            return getUserInfo().then(function (user) {
                if (!user.roles || !user.roles.length) {
                    return false;
                }
                var i;
                var result = false;
                for (i = 0; i < arrayOfPermissions.length; ++i) {
                    if (user.roles.indexOf(arrayOfPermissions[i]) !== -1) {
                        result = true;
                        break;
                    }
                }
                return result;
            });
        }
    }

    return $iqApiAuth;
});
