define([], function () {
    'use strict';

    $iqApiDict.$inject = ['$http', '$iq.api'];
    function $iqApiDict($http, $iqApi) {
        var service = {};

        service.getByName = getByName;
        service.getByEnumName = getByEnumName;

        return service;

        ////////////////////

        function getByName(name) {
            return $iqApi('get', '/dictionaries/' + name)
                .then(function (response) {
                    return response.data;
                });
        }

        function getByEnumName(enumName) {
            return $iqApi('get', '/dictionaries/enum/' + enumName)
                .then(function (response) {
                    return response.data;
                });
        }
    }

    return $iqApiDict;
});
