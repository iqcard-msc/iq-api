define([
    'angular',
    './iq-api.service',
    './iq-api.auth.service',
    './iq-api.dict.service'
], function (
    angular,
    $iqApi,
    $iqApiAuth,
    $iqApiDict
) {
    'use strict';

    var app = angular.module('iq.api', []);

    app.provider('$iq.api', $iqApi);

    app.factory('$iq.api.auth', $iqApiAuth);
    app.factory('$iq.api.dict', $iqApiDict);

    return app;
});
