var http = require('http');
var url = require('url');

http.createServer(function (req, res) {

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Credentials', true);
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');

    var parsedUrl = url.parse(req.url, true);

    var resp = {
        url: parsedUrl.pathname,
        method: req.method,
        params: parsedUrl.query,
        headers: req.headers,
        body: ''
    };

    req.on('data', function (chunk) {
        resp.body += chunk;
    });

    req.on('end', function () {
        console.log(resp);
        res.end(JSON.stringify(resp));
    });
}).listen(8080, '127.0.0.1');
