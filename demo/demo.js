require.config({
    paths: {
        'angular': '../bower_components/angular/angular',
        'domReady': 'https://cdnjs.cloudflare.com/ajax/libs/require-domReady/2.0.1/domReady',
        'iq-api': '../iq-api'
    },
    shim: {
        'angular': {
            exports: 'angular'
        }
    }
});

require(['domReady!', 'angular', 'iq-api'], function (document, angular) {
    var app = angular.module('demo', [
        'iq.api'
    ]);

    app.config(['$iq.apiProvider',
        function ($iqApiProvider) {
            $iqApiProvider.urlPrefix = 'http://localhost:9000/@@api';
        }
    ]);

    app.controller('DemoController', ['$http', '$iq.api', '$iq.api.auth', '$iq.api.dict',
        function ($http, $iqApi, $iqApiAuth, $iqApiDict) {
            var vm = this;
            vm.response = {};
            vm.form = {};

            vm.get = function () {
                $iqApi('post', '/get', {data: {foo:'bar', baz: 1}})
                    .then(writeResponseData);
            };

            vm.post = function () {
                $iqApi.wrap($http.post, '/bar')({data: {foo: 'bar'}})
                    .then(writeResponseData);
            };

            vm.postForm = function () {
                $iqApi.postForm('/baz', {username: 'user', password: 'password'})
                    .then(writeResponseData);
            };

            // Auth

            vm.login = function () {
                $iqApiAuth.login(vm.form)
                    .then(writeResponseData);
            };

            vm.logout = function () {
                $iqApiAuth.logout()
                    .then(writeResponseData);
            };

            vm.getUserInfo = function () {
                $iqApiAuth.getUserInfo()
                    .then(writeResponse);
            };

            // Dicts

            vm.getDictByName = function () {
                $iqApiDict.getByName('name')
                    .then(writeResponse);
            };

            vm.getDictByEnumName = function () {
                $iqApiDict.getByEnumName('enumName')
                    .then(writeResponse);
            };

            ////////////////////

            function writeResponseData(response) {
                vm.response = response.data;
            }

            function writeResponse(response) {
                vm.response = response;
            }
        }
    ]);

    angular.bootstrap(document, ['demo']);
});
